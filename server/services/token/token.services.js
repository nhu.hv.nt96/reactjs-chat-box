const jwt = require('jsonwebtoken')
const AppConf = require('../../config/app.config.json')

class TokenService {
  constructor() {
    this.data = new Map()
  }

  singToken(user) {
    const token = jwt.sign({ user: user }, AppConf.tokenKey, { expiresIn: '1d' })
    this.data.set(token, token)
    return token
  }

  verifyToken(token) {
    const user = jwt.verify(token, AppConf.tokenKey)
    return user
  }

  removeToken(token) {
    this.data.delete(token)
  }
}


module.exports = new TokenService()
