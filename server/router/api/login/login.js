const express = require('express')
const router = express.Router()
const User = require('../../../models/user/user')
const tokenService = require('../../../services/token/token.services')

router.post('/', async (req, res) => {
  const { username, password } = req.body
  User.findOne({ username: username, password: password }, (err, user) => {
    if (err) {
      return res.json({ err })
    }
    if (!user) {
      return res.json({
        code: "LOGIN_4000",
        data: {},
        message: "login failed"
      })
    }

    const token = tokenService.singToken(user)
    console.log("dawdaw", token)
    res.json({
      code: "LOGIN_2000",
      message: "login success",
      data: {},
      token
    })
  })
})

module.exports = router