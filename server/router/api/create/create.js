const express = require('express')
const User = require('../../../models/user/user')
const router = express.Router()

router.post('/', (req, res) => {
  const { fullname, username, password } = req.body
  console.log('fullname', fullname)
  const create = new User({
    fullname: fullname,
    username: username,
    password: password
  })

  create.save()
    .then(data => {
      res.json(data)
    })
    .catch(err => {
      res.json({ message: err })
    })
})

module.exports = router