const express = require('express')
const mongoose = require('mongoose')
const http = require('http')
const bodyParser = require('body-parser')
const app = express()
const server = http.createServer(app)
const PORT = process.env.PORT || 4000
const Login = require('./router/api/login/login')
const Logout = require('./router/api/logout/logout')
const Create = require('./router/api/create/create')

// mongoose.connect('mongodb+srv://nhuhuynh:Aa123123@cluster0-hyzyh.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true })
// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'connection error:'));
// db.once('open', function () {
//       console.log('Connected to MongoDB');
//   });

  mongoose
     .connect('mongodb+srv://nhuhuynh:Aa123123@cluster0-hyzyh.mongodb.net/reactJS?retryWrites=true&w=majority', { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true })
     .then(() => console.log( 'Database Connected' ))
     .catch(err => console.log( err ));



app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use('/api/login', Login)
app.use('/api/logout', Logout)
app.use('/api.create', Create)
server.listen(PORT, () => {
  console.log(`server started port ${PORT}`)
})